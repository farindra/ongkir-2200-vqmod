<?php
class ControllerCheckoutShippingMethod extends Controller {
	public function index() {
		$this->load->language('checkout/checkout');

		if (isset($this->session->data['shipping_address'])) {
			// Shipping Methods
			$method_data = array();

			$this->load->model('extension/extension');

			$results = $this->model_extension_extension->getExtensions('shipping');

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('shipping/' . $result['code']);

					$quote = $this->{'model_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);

					if ($quote) {
						$method_data[$result['code']] = array(
							'title'      => $quote['title'],
							'quote'      => $quote['quote'],
							'sort_order' => $quote['sort_order'],
							'error'      => $quote['error']
						);
					}
				}
			}

			if ($this->config->get('ongkir_status')) {

				$deData = $this->getOngkir();
				if (!empty($deData)) {
					array_push($method_data, $deData) ;
				}
				
			}
			

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);

			$this->session->data['shipping_methods'] = $method_data;
		}

		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_continue'] = $this->language->get('button_continue');

		if (empty($this->session->data['shipping_methods'])) {
			$data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['shipping_methods'])) {
			$data['shipping_methods'] = $this->session->data['shipping_methods'];
		} else {
			$data['shipping_methods'] = array();
		}

		if (isset($this->session->data['shipping_method']['code'])) {
			$data['code'] = $this->session->data['shipping_method']['code'];
		} else {
			$data['code'] = '';
		}

		if (isset($this->session->data['comment'])) {
			$data['comment'] = $this->session->data['comment'];
		} else {
			$data['comment'] = '';
		}

		$this->response->setOutput($this->load->view('checkout/shipping_method', $data));
	}

	public function save() {
		$this->load->language('checkout/checkout');

		$json = array();

		// Validate if shipping is required. If not the customer should not have reached this page.
		if (!$this->cart->hasShipping()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate if shipping address has been set.
		if (!isset($this->session->data['shipping_address'])) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', true);
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}

		if (!isset($this->request->post['shipping_method'])) {
			$json['error']['warning'] = $this->language->get('error_shipping');
		} else {
			$shipping = explode('.', $this->request->post['shipping_method']);

			if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
				$json['error']['warning'] = $this->language->get('error_shipping');
			}
		}

		if (!$json) {
			$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];

			$this->session->data['comment'] = strip_tags($this->request->post['comment']);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

        function getOngkir() {
          $tujuan = $this->session->data['shipping_address'];
          $tujuan = $tujuan['city'];
          $deID = '0';

          //$qry="SELECT * FROM `" . DB_PREFIX . "setting` WHERE `key` = 'ongkir_city'";
          $query = $this->db->query("SELECT value FROM `" . DB_PREFIX . "setting` WHERE `key` = 'ongkir_city'");
          $data = $query->rows;
          //print_r($data[0]['value']).'13';
          $data = json_decode($data[0]['value'],true);

          
          $tujuan = preg_replace('/\s+/', '', $tujuan);

                foreach ($data as $key => $value) {
                  $deVal=$value['city_name'] . '  (' . $value['type'] . ')' ;
                  $deVal=preg_replace('/\s+/', '', $deVal);

                  if ($tujuan==$deVal){
                    $deID = $value['city_id'];
                    break;
                  }

                }
          
          $weight = $this->weight->convert($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->config->get('config_weight_class_id'));
          $weight_code = strtoupper($this->weight->getUnit($this->config->get('config_weight_class_id')));

          $options = array(
			        'http' => array(
			        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			        'method'  => 'POST'
			    )
			);


          $context  = stream_context_create($options);
          $deRest = file_get_contents('http://localhost:8000/api/ongkir/cost/456/'.$deID.'/'.($weight*1000).'/jne',false,$context); 
          $deRest = json_decode($deRest,true);
          //print_r($deRest);

          $method_data = array();
          $quote_data = array();
          if ($deRest['status']){

          	foreach ($deRest['values'][0]['costs'] as $key => $value) {
          		
          		if ($value['service']=='OKE' || $value['service']=='REG' || $value['service']=='CTCOKE' || $value['service']=='CTC' || $value['service']=='CTCYES') {
          			echo $value['service'].$key.'</br>';	
		            
		            

		            $quote_data[$value['service']] = array(
		              'code'         => 'JNE.'.$value['service'],
		              'title'        => 'JNE '.$value['service']. ' ' .$value['description'] ,
		              'cost'         => $value['cost'][0]['value'],
		              'tax_class_id' => $this->config->get('ongkir_tax_class_id'),
		              'text'         => $this->currency->format($this->tax->calculate($value['cost'][0]['value'], $this->config->get('ongkir_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
		            );

		            $method_data = array(
		              'code'       => $value['service'],
		              'title'      => 'JNE.'.$value['service'],
		              'quote'      => $quote_data,
		              'sort_order' => $this->config->get('ongkir_sort_order'),
		              'error'      => false
		            );

		            $quote = $method_data;

		            $method_data[$value['service']] = array(
							'title'      => $quote['title'],
							'quote'      => $quote['quote'],
							'sort_order' => $quote['sort_order'],
							'error'      => $quote['error']
						);
		        }
        	}
          } 

          print_r($method_data);

          return $method_data;
        }
      
    
}