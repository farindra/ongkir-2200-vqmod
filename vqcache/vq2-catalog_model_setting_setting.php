<?php
class ModelSettingSetting extends Model {
	public function getSetting($code, $store_id = 0) {
		$data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");

		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$data[$result['key']] = $result['value'];
			} else {
				$data[$result['key']] = json_decode($result['value'], true);
			}
		}

		return $data;
	}

      public function getCityByZoneId($zone_id, $store_id=0) {
        
        $data = array();
        $json=array();
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = 'ongkir_city'");
        $data = $query->row;
        
        
        if (empty($data)){

          $data =$this->addCity($store_id);
          
        }
          //var_dump($data);
          $data = json_decode($data['value'],true);

          foreach ($data as $key => $value) {
            if ($value['province_id']==$zone_id){
              array_push($json,array('city_name'=> $value['city_name'] . '  (' . $value['type'] . ')'  ,'city_id'=> $value['city_id']));
            }
          }
        

        $json = array('city'=> $json );

        return $json;
      }      

      public function addCity($store_id){

        $this->addZone();

        $data = file_get_contents('http://localhost:8000/api/ongkir/files/city/json'); // POST request
        $qry="INSERT INTO  `" . DB_PREFIX . "setting` (`setting_id`,`store_id`,`code`,`key`,`value`,`serialized`) values(NULL,'0','ongkir','ongkir_city','".json_encode((json_decode($data,true)['values']))."','1')";
        $query = $this->db->query($qry);
        
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = 'ongkir_city'");
        $data = $query->row;

        return $data;
      } 

      public function addZone(){
        $qry="CREATE TABLE `". DB_PREFIX ."zone_old` LIKE `". DB_PREFIX ."zone`";//;TRUNCATE TABLE `". DB_PREFIX ."zone`;";
        
        //$data = file_get_contents('http://localhost:8000/api/ongkir/files/province/json'); // POST request
        //$qry="";
        //$qry="INSERT INTO  `" . DB_PREFIX . "setting` (`setting_id`,`store_id`,`code`,`key`,`value`,`serialized`) values(NULL,'0','ongkir','ongkir_city','".json_encode((json_decode($data,true)['values']))."','1')";
        //echo $qry;
        $query = $this->db->query($qry);
        //$data = $query->row;

        $qry="INSERT INTO `". DB_PREFIX ."zone_old` SELECT * FROM `". DB_PREFIX ."zone`";
        $query = $this->db->query($qry);
        //$data = $query->row;


        $qry="TRUNCATE TABLE `". DB_PREFIX ."zone`";
        $query = $this->db->query($qry);
        //$data = $query->row;

        $data = file_get_contents('http://localhost:8000/api/ongkir/files/province/json'); // POST request
        $data = json_decode($data,true);

        foreach ($data['values'] as $key => $value) {
          //array_push($json,array('city_name'=> $value['city_name'] . '  (' . $value['type'] . ')'  ,'city_id'=> $value['city_id']));
          $qry="INSERT INTO `". DB_PREFIX ."zone` (`zone_id`,`country_id`,`name`,`code`,`status`) VALUES (NULL, '100','".$value['province']."','','1')";
          $query = $this->db->query($qry);
          //$deRun = $query->row;

        }
      }
      
    
}