<?php
class ControllerCommonDashboard extends Controller {

      private $error = array();
      
    
	public function index() {
		$this->load->language('common/dashboard');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_map'] = $this->language->get('text_map');
		$data['text_activity'] = $this->language->get('text_activity');
		$data['text_recent'] = $this->language->get('text_recent');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		// Check install directory exists
		if (is_dir(dirname(DIR_APPLICATION) . '/install')) {
			$data['error_install'] = $this->language->get('error_install');
		} else {
			$data['error_install'] = '';
		}

		$data['token'] = $this->session->data['token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['order'] = $this->load->controller('dashboard/order');
		$data['sale'] = $this->load->controller('dashboard/sale');
		$data['customer'] = $this->load->controller('dashboard/customer');
		$data['online'] = $this->load->controller('dashboard/online');
		$data['map'] = $this->load->controller('dashboard/map');
		$data['chart'] = $this->load->controller('dashboard/chart');
		$data['activity'] = $this->load->controller('dashboard/activity');
		$data['recent'] = $this->load->controller('dashboard/recent');
		$data['footer'] = $this->load->controller('common/footer');

      if (isset($this->request->get['ongkir'])) {
        
        $deApi = 'http://localhost:8000/';

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

          $data=file_get_contents($deApi.'api/ongkir/files/city/json'); 
          $this->request->post['ongkir_city'] = json_encode((json_decode($data,true)['values']));

          $this->model_setting_setting->editSetting('ongkir', $this->request->post);

          $this->session->data['success'] = 'Data Berhasil disimpan';

          $this->response->redirect($this->url->link('common/dashboard&ongkir=1', 'token=' . $this->session->data['token'], true));
        }

        $data['ongkir'] = '1' ;
        $data['heading_title'] = 'Ongkir' ;
        $data['button_save'] = 'Simpan' ;
        $data['button_cancel'] = 'Batal' ;
                
        $data['entry_status'] = 'Status' ;
        $data['entry_tax_class'] = 'Tax Class' ;
        $data['entry_geo_zone'] = 'Geo Zone' ;
        $data['entry_sort_order'] = 'Sort Order' ;
        $data['entry_license'] = 'License Key' ;
        $data['entry_ongkir_city'] = 'Cities' ;

        $data['text_edit'] = 'Edit - Ongkir' ;
        $data['text_enabled'] = 'Enable' ;
        $data['text_disabled'] = 'Disable' ;
        $data['text_all_zones'] = 'All Zone' ;
        $data['text_none'] = '--- None ---' ;
        //$data['text_order'] = '0' ;
        //$data['text_license'] = 'free' ;
        $data[''] = '' ;

        // license

        if (isset($this->error['warning'])) {
          $data['error_warning'] = $this->error['warning'];
        } else {
          $data['error_warning'] = '';
        }

        $data['action'] = $this->url->link('common/dashboard&ongkir=1', 'token=' . $this->session->data['token'], true);

        $data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true);

        //if (isset($this->request->post['ongkir_cost'])) {
        //  $data['ongkir_cost'] = $this->request->post['ongkir_cost'];
        //} else {
        //  $data['ongkir_cost'] = $this->config->get('ongkir_cost');
        //}

        if (isset($this->request->post['ongkir_status'])) {
          $data['ongkir_status'] = $this->request->post['ongkir_status'];
        } else {
          
          $query = $this->db->query("SELECT * FROM `".DB_PREFIX."setting` WHERE `key`='ongkir_status'");
          if (!$query->num_rows) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`setting_id`,`store_id`,`code`,`key`,`value`,`serialized`) VALUES (NULL,'0','ongkir','ongkir_status','0','0')");
            $this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`setting_id`,`store_id`,`code`,`key`,`value`,`serialized`) VALUES (NULL,'0','ongkir','ongkir_tax_class_id','0','0')");
            $this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`setting_id`,`store_id`,`code`,`key`,`value`,`serialized`) VALUES (NULL,'0','ongkir','ongkir_geo_zone_id','0','0')");
            $this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`setting_id`,`store_id`,`code`,`key`,`value`,`serialized`) VALUES (NULL,'0','ongkir','ongkir_license','free','0')");
            $this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`setting_id`,`store_id`,`code`,`key`,`value`,`serialized`) VALUES (NULL,'0','ongkir','ongkir_sort_order','1','0')"); 

            $query = $this->db->query("SELECT * FROM `".DB_PREFIX."setting` WHERE `key`='ongkir_city'");
            if (!$query->num_rows) {
              $data = file_get_contents($deApi.'api/ongkir/files/city/json'); // POST request

              $this->db->query("INSERT INTO  `" . DB_PREFIX . "setting` (`setting_id`,`store_id`,`code`,`key`,`value`,`serialized`) values(NULL,'0','ongkir','ongkir_city','".json_encode((json_decode($data,true)['values']))."','1')");
            }

            $query = $this->db->query("SHOW TABLES LIKE 'oc_2200zone_old'");
            if (!$query->num_rows) {
              $this->db->query("CREATE TABLE `". DB_PREFIX ."zone_old` LIKE `". DB_PREFIX ."zone`");
              $this->db->query("INSERT INTO `". DB_PREFIX ."zone_old` SELECT * FROM `". DB_PREFIX ."zone`");
              $data = file_get_contents($deApi.'api/ongkir/files/province/json'); // POST request
              $data = json_decode($data,true);

              foreach ($data['values'] as $key => $value) {
                $this->db->query("INSERT INTO `". DB_PREFIX ."zone` (`zone_id`,`country_id`,`name`,`code`,`status`) VALUES (NULL, '100','".$value['province']."','','1')");
              }
            }

             $this->response->redirect($this->url->link('common/dashboard&ongkir=1', 'token=' . $this->session->data['token'], true));       
          }
          $query = '';
          $data['ongkir_status'] = $this->config->get('ongkir_status');
        }
        

        if (isset($this->request->post['ongkir_tax_class_id'])) {
          $data['ongkir_tax_class_id'] = $this->request->post['ongkir_tax_class_id'];
        } else {
          $data['ongkir_tax_class_id'] = $this->config->get('ongkir_tax_class_id');
        }

        $this->load->model('localisation/tax_class');

        $data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

        if (isset($this->request->post['ongkir_geo_zone_id'])) {
          $data['ongkir_geo_zone_id'] = $this->request->post['ongkir_geo_zone_id'];
        } else {
          $data['ongkir_geo_zone_id'] = $this->config->get('ongkir_geo_zone_id');
        }

        $this->load->model('localisation/geo_zone');

        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        if (isset($this->request->post['ongkir_license'])) {
          $data['ongkir_license'] = $this->request->post['ongkir_license'];
        } else {
          $data['ongkir_license'] = $this->config->get('ongkir_license');
        }

        if (isset($this->request->post['ongkir_sort_order'])) {
          $data['ongkir_sort_order'] = $this->request->post['ongkir_sort_order'];
        } else {
          $data['ongkir_sort_order'] = $this->config->get('ongkir_sort_order');
        }

        if (isset($this->request->post['ongkir_city'])) {
          $data['ongkir_city'] = $this->request->post['ongkir_city'];
        } else {
          $data['ongkir_city'] = $this->config->get('ongkir_city');
        }

      }

      
      
    

		// Run currency update
		if ($this->config->get('config_currency_auto')) {
			$this->load->model('localisation/currency');

			$this->model_localisation_currency->refresh();
		}

		$this->response->setOutput($this->load->view('common/dashboard', $data));
	}

      protected function validate() {
        if (!$this->user->hasPermission('modify', 'shipping/flat')) {
          $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
      }
      
    
}